## Project idea

The last idea is an app that can be used anytime, anywhere to query the status of a package. 
When a package is sent, the app automatically tracks the process of the package which includes retrieving data from each mailing company and automatically adds the package to a list which matches a mobile phone number or physical address of the sender and recipient where they can see the location of the package at all times using the app.


On a Chinese website, we can query each package according to the mobile phone number or checking number. The mobile phone number can automatically match the package and add it to the package list without manual addition.

Everyone has the habit of online shopping. 
Sometimes we buy different things on different websites at the same time. When we check the status of packages, we need to click on the links in every email or log on to the purchase website, which is a little cumbersome. 
If there is an app that can merge each package into the same list, it will be easy to query the status of the package.

The main function is to query the status of the package.

I need to use API to connect to every express company for easy inquiry. 
<a href="https://www.shipengine.com/welcome2?ref=se-gppc_onlinepostage-developer_shipping+api&utm_source=google&utm_medium=sem&utm_campaign=onlinepostage-developer&gclid=EAIaIQobChMItZnE8dGA4AIVMRh9Ch0P3wXQEAAYAiAAEgKx5fD_BwE">API</a>

